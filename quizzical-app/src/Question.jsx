import React, { useState, useEffect } from "react";
import { htmlToText } from "html-to-text";

export default function Question(props) {
  const [answers, setAnswers] = useState(
    shuffle([...props.incorrect, props.correct])
  );

  function shuffle(array) {
    return array.sort(() => Math.random() - 0.5);
  }

  const [choice, setChoice] = useState("");
  function setActive(e) {
    setChoice(e.target.innerText);
  }

  function changeAnswerClass(answerOption) {
    let answerClass = "btn btn-answer";
    if (answerOption === choice) {
      answerClass = "btn btn-answer btn-answer--active";
    }
    if (props.results && answerOption === props.correct) {
      answerClass = "btn btn-answer btn-answer--correct";
    }
    if (
      props.results &&
      answerOption !== props.correct &&
      choice === answerOption
    ) {
      answerClass = "btn btn-answer btn-answer--incorrect";
    }
    if (
      props.results &&
      answerOption !== props.correct &&
      choice !== answerOption
    ) {
      answerClass = "btn btn-answer btn-answer--other";
    }
    return answerClass;
  }

  useEffect(() => {
    props.setMatches((prevScore) => {
      if (props.results && props.correct === choice) {
        return prevScore + 1;
      } else {
        return prevScore;
      }
    });
  }, [props.results]);

  return (
    <div className="question-block">
      <h2 className="question">{htmlToText(props.question)}</h2>
      <div className="answers">
        {answers.map((answer, index) => (
          <a
            className={changeAnswerClass(answer)}
            onClick={setActive}
            key={index}
          >
            {htmlToText(answer)}
          </a>
        ))}
      </div>
    </div>
  );
}
