import React from "react";

function Footer() {
  return (
    <footer className="footer">
      <a href="#">
        <img src="src/assets/social/twitter.png" />
      </a>
      <a href="#">
        <img src="src/assets/social/fb.png" />
      </a>
      <a href="https://www.instagram.com/tania.boiar/" target="_blank">
        <img src="src/assets/social/inst.png" />
      </a>
      <a href="#">
        <img src="src/assets/social/github.png" />
      </a>
    </footer>
  );
}

export default Footer;
