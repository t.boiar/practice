import React from "react";

function Header() {
  return (
    <div className="header">
      <img className="header-img" src="src/assets/avatar.jpeg" />
      <h1 className="header-title">Tetiana Boiar</h1>
      <h2 className="header-subtitle">Frontend Developer</h2>
      <a className="header-link" href="https://gitlab.com/t.boiar">
        gitlab.com/t.boiar
      </a>
      <div className="header-buttons">
        <a
          href="mailto:taniavasylchyk@gmail.com"
          className="button button-email"
        >
          <img
            className="button-icon"
            src="src/assets/button-icons/email.png"
          />
          Email
        </a>
        <a
          href="https://ua.linkedin.com/in/tetiana-boiar-0701b8187"
          className="button button-ln"
          target="_blank"
        >
          <img className="button-icon" src="src/assets/button-icons/ln.png" />
          LinkedIn
        </a>
      </div>
    </div>
  );
}

export default Header;
