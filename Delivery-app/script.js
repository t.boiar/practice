"use strict";

// prettier-ignore

// const shopsItems = document.querySelectorAll('.shops__item');
const shopsList = document.querySelector('.shops__list');

const productCardsList = document.querySelector(".product-cards");

const toBuyButtons = document.querySelectorAll(".to-buy");

async function fetchAsync(url) {
  let response = await fetch(url);
  let data = await response.json();
  return data;
}

const shops = fetchAsync("http://localhost:3000/shops");

let productCard;
const renderFoods = function (foods) {
  productCardsList.innerHTML = "";
  for (const food of foods) {
    productCard = document.createElement("div");
    productCard.className = "product-card";
    // productCard.innerHTML = food.name;
    productCard.innerHTML = `
		<div class="product-card__content">
			<img class="product-img" src=${food.img} alt="Pizza" />
			<p class="product-name">${food.name}</p>
		</div>
		<button class="to-buy">Add to cart</button>`;
    console.log(productCard);
    productCard.id = food.name;
    productCardsList.appendChild(productCard);
  }
};

let shopButtons;
shops.then((data) => {
  for (const shop of data) {
    shopButtons = document.createElement("button");
    shopButtons.type = "button";
    shopButtons.className = "shop-button";
    shopButtons.innerHTML = shop.name;
    shopButtons.value = shop.id;
    shopsList.appendChild(shopButtons);

    shopButtons.addEventListener("click", function (e) {
      console.log(e.target.value);

      const food = fetchAsync(`http://localhost:3000/food/${e.target.value}`);
      console.log(food);
      food.then((data) => renderFoods(data));
    });
  }
});

// map
if (navigator.geolocation)
  navigator.geolocation.getCurrentPosition(
    function (position) {
      const { latitude } = position.coords;
      const { longitude } = position.coords;
      console.log(`https://www.google.com/maps/@${latitude},${longitude}`);

      const coords = [latitude, longitude];

      const map = L.map("map").setView(coords, 13);

      L.tileLayer("https://{s}.tile.openstreetmap.fr/hot//{z}/{x}/{y}.png", {
        attribution:
          '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      }).addTo(map);
    },
    function () {
      console.log("Could not get your position");
    }
  );
