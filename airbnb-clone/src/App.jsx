import React from "react";
import Navbar from "./components/Navbvar";
import Hero from "./components/Hero";
import Card from "./components/Card";
import cardsData from "./data";

function App() {
  const cards = cardsData.map((card) => {
    return (
      <Card
        key={card.id}
        // card={card}
        {...card}
      />
    );
  });

  return (
    <div>
      <Navbar />
      <Hero />
      <div className="cards-list">{cards}</div>
    </div>
  );
}

export default App;
