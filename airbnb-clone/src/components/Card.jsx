import React from "react";
import star from "/public/assets/star.png";

function Card(props) {
  let badgeText;
  // let photo = props.card.coverImg;
  // let rating = props.card.stats.rating;
  // let reviews = props.card.stats.reviewCount;
  // let location = props.card.location;
  // let title = props.card.title;
  // let price = props.card.price;
  // let openSpots = props.card.openSpots;

  if (!props.openSpots) {
    badgeText = "Sold out";
  } else if (props.location === "Online") {
    badgeText = "Online";
  }

  return (
    <div className="card">
      {badgeText && <div className="card-badge">{badgeText}</div>}
      <img className="card-img" src={`public/assets/${props.coverImg}`} />
      <div className="card-stats">
        <img className="card-star" src={star} />
        <span>{props.stats.rating}</span>
        <span className="gray">({props.stats.reviewCount}) •</span>
        <span className="gray">{props.location}</span>
      </div>
      <p>{props.title}</p>
      <p>
        <span className="bold">From ${props.price}</span> / person
      </p>
    </div>
  );
}

export default Card;
