import React, { useEffect, useState } from "react";
import Die from "./Die";
import { nanoid } from "nanoid";
import ReactConfetti from "react-confetti";

function App() {
  const [dice, setValue] = useState(allNewDice());
  const [tenzies, setTenzies] = useState(false);

  useEffect(() => {
    const allHeld = dice.every((die) => die.isHeld);
    const firstValue = dice[0].value;
    const allSameValue = dice.every((die) => die.value === firstValue);
    if (allHeld && allSameValue) {
      setTenzies(true);
    }
  }, [dice]);

  function generateNewDie() {
    return {
      value: Math.ceil(Math.random() * 6),
      isHeld: false,
      id: nanoid(),
    };
  }

  function allNewDice() {
    const allDice = [];
    for (let i = 0; i < 10; i++) {
      allDice.push(generateNewDie());
    }
    return allDice;
  }

  function rollDice() {
    if (!tenzies) {
      setValue((oldDice) =>
        oldDice.map((die) => {
          return die.isHeld ? die : generateNewDie();
        })
      );
    } else {
      setTenzies(false);
      setValue(allNewDice());
    }
  }

  function holdDice(id) {
    setValue((oldDice) =>
      oldDice.map((die) => {
        return die.id === id ? { ...die, isHeld: !die.isHeld } : die;
      })
    );
  }

  const diceElements = dice.map((die) => (
    <Die
      key={die.id}
      value={die.value}
      isHeld={die.isHeld}
      holdDice={() => holdDice(die.id)}
    />
  ));

  return (
    <div className="App">
      <main className="main">
        {tenzies && <ReactConfetti />}
        <div className="playground-wrap">
          <div className="playground">
            <h1 className="headline">Tenzies</h1>
            <p className="description">
              Roll until all dice are the same. Click each die to freeze it at
              its current value between rolls.
            </p>
          </div>
          <div className="dice-wrap">{diceElements}</div>
          <button onClick={rollDice} className="btn-roll">
            {tenzies ? "New Game" : "Roll"}
          </button>
        </div>
      </main>
    </div>
  );
}

export default App;
