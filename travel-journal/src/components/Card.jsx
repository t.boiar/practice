import React from "react";
import map_pin from "/public/assets/pin.png";

function Card(props) {
  return (
    <div className="card">
      <img className="card-photo" src={props.imageUrl} />
      <div className="card-content">
        <div className="card-location">
          <img className="card-img_pin" src={map_pin} />
          <span className="card-country">{props.location}</span>
          <a className="card-link" href={props.googleMapsUrl} target="_blank">
            View on Google Maps
          </a>
          <h2 className="card-title">{props.title}</h2>
          <h3 className="card-date">
            {props.startDate} - {props.endDate}
          </h3>
          <p className="card-text">{props.description}</p>
        </div>
      </div>
    </div>
  );
}

export default Card;
