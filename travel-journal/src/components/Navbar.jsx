import React from "react";
import logo from "/public/assets/travel-logo.png";

function Navbar() {
  return (
    <nav className="navbar">
      <img className="logo" src={logo} />
      <h1 className="logo-title">my travel journal</h1>
    </nav>
  );
}

export default Navbar;
